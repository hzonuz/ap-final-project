package com.example.apfinalproject;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


public class OthelloActivity extends AppCompatActivity implements View.OnClickListener {
    private Button[][] buttons = new Button[8][8];
    private char[][] board = new char[8][8];
    private int p1Score = 0;
    private int p2Score = 0;
    private int round;
    boolean p1Turn = true;
    TextView p1ScoreView;
    TextView p2ScoreView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.othello_layout);
        round = 0;
        for(int i = 0 ; i < 8 ;i++) {
            for(int j = 0 ; j < 8 ; j++) {
                String bId = "othello_button_" + i + j;
                int res = getResources().getIdentifier(bId,"id",getPackageName());
                buttons[i][j] = findViewById(res);
                buttons[i][j].setOnClickListener(this);
            }
        }
        p1ScoreView = findViewById(R.id.p1_score_othello);
        p2ScoreView = findViewById(R.id.p2_score_othello);
        buttons[3][4].setText("I");
        buttons[4][3].setText("I");
        buttons[3][3].setText("O");
        buttons[4][4].setText("O");
        board[3][4] = board[4][3] = 'I';
        board[4][4] = board[3][3] = 'O';
    }

    @Override
    public void onClick(View v) {
        if(((Button) v).getText().equals('O') || ((Button) v).getText().equals('I')) {
            Log.i("Click","Return !");
            return;
        }
        int x = 0 , y = 0;
        for(int i = 0; i < 8 ; i++) {
            for(int j = 0 ; j < 8 ; j++) {
                String bId = "othello_button_" + i + j;
                int res = getResources().getIdentifier(bId,"id",getPackageName());
                if(v.getId() == res) {
                    x = i;
                    y = j;
                    Log.i("Click","X , Y founded !");
                }
            }
        }
        if(p1Turn && reverse(x,y,board,p1Turn)) {
            board[x][y] = 'O';
            p1Turn = false;
        }
        else if (!p1Turn && reverse(x,y,board,p1Turn)) {
            board[x][y] = 'I';
            p1Turn = true;
        }
        else {
            return;
        }
        round++;
        for(int i = 0; i < 8 ; i++) {
            for (int j = 0; j < 8; j++) {
                buttons[i][j].setText(Character.toString(board[i][j]));
                if(board[i][j] == 'O') {
                    p1Score++;
                } else if(board[i][j] == 'I') {
                    p2Score++;
                }
            }
        }
        if(round == 60) {
            if(p1Score > p2Score) {
                completeBoard("P1 Wins !");
            }
            else if(p1Score < p2Score) {
                completeBoard("P2 Wins !");
            } else {
                completeBoard("Draw !");
            }
        }
        p1ScoreView.setText("Player 1 : " + Integer.toString(p1Score));
        p2ScoreView.setText("Player 2 : " + Integer.toString(p2Score));
        p1Score = 0;
        p2Score = 0;
    }

    static boolean reverse (int x , int y , char[][] b , boolean o) {
        boolean rev = false, isOpp = false;
        if (o) {
            for (int i = x + 1; i < 8; i++) {
                if (b[i][y] == 'I') {
                    isOpp = true;
                }
                else if (b[i][y] == 'O' && isOpp) {
                    for(int j = i; j>x ; j--) {
                        b[j][y] = 'O';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = x - 1; i > -1; i--) {
                if (b[i][y] == 'I') {
                    isOpp = true;
                }
                else if (b[i][y] == 'O' && isOpp) {
                    for(int j = i; j < x ; j++) {
                        b[j][y] = 'O';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = y + 1; i < 8; i++) {
                if (b[x][i] == 'I') {
                    isOpp = true;
                }
                else if (b[x][i] == 'O' && isOpp) {
                    for(int j = i; j > y ; j--) {
                        b[x][j] = 'O';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = y - 1; i > -1; i--) {
                if (b[x][i] == 'I') {
                    isOpp = true;
                }
                else if (b[x][i] == 'O' && isOpp) {
                    for(int j = i; j < y ; j++) {
                        b[x][j] = 'O';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = x + 1 , j = y + 1; i < 8 && j < 8; j++, i++) {
                if (b[i][j] == 'I') {
                    isOpp = true;
                }
                else if (b[i][j] == 'O' && isOpp) {
                    for(int k = i , t = j ; k > x && t > y ; k-- , t--) {
                        b[k][t] = 'O';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = x - 1 , j = y - 1; i > -1 && j > -1; j--, i--) {
                if (b[i][j] == 'I') {
                    isOpp = true;
                }
                else if (b[i][j] == 'O' && isOpp) {
                    for(int k = i , t = j ; k < x && t < y ; k++ , t++) {
                        b[k][t] = 'O';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = x + 1 , j = y - 1; i < 8 && j > -1; j--, i++) {
                if (b[i][j] == 'I') {
                    isOpp = true;
                }
                else if (b[i][j] == 'O' && isOpp) {
                    for(int k = i , t = j ; k > x && t < y ; k-- , t++) {
                        b[k][t] = 'O';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = x - 1 , j = y + 1; i > -1 && j < 8; j++, i--) {
                if (b[i][j] == 'I') {
                    isOpp = true;
                }
                else if (b[i][j] == 'O' && isOpp) {
                    for(int k = i , t = j ; k < x && t > y ; k++ , t--) {
                        b[k][t] = 'O';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
        }
        else {
            for (int i = x + 1; i < 8; i++) {
                if (b[i][y] == 'O') {
                    isOpp = true;
                }
                else if (b[i][y] == 'I' && isOpp) {
                    for(int j = i; j>x ; j--) {
                        b[j][y] = 'I';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = x - 1; i > -1; i--) {
                if (b[i][y] == 'O') {
                    isOpp = true;
                }
                else if (b[i][y] == 'I' && isOpp) {
                    for(int j = i; j < x ; j++) {
                        b[j][y] = 'I';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = y + 1; i < 8; i++) {
                if (b[x][i] == 'O') {
                    isOpp = true;
                }
                else if (b[x][i] == 'I' && isOpp) {
                    for(int j = i; j > y ; j--) {
                        b[x][j] = 'I';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = y - 1; i > -1; i--) {
                if (b[x][i] == 'O') {
                    isOpp = true;
                }
                else if (b[x][i] == 'I' && isOpp) {
                    for(int j = i; j < y ; j++) {
                        b[x][j] = 'I';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = x + 1 , j = y + 1; i < 8 && j < 8; j++, i++) {
                if (b[i][j] == 'O') {
                    isOpp = true;
                }
                else if (b[i][j] == 'I' && isOpp) {
                    for(int k = i , t = j ; k > x && t > y ; k-- , t--) {
                        b[k][t] = 'I';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = x - 1 , j = y - 1; i > -1 && j > -1; j--, i--) {
                if (b[i][j] == 'O') {
                    isOpp = true;
                }
                else if (b[i][j] == 'I' && isOpp) {
                    for(int k = i , t = j ; k < x && t < y ; k++ , t++) {
                        b[k][t] = 'I';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = x + 1 , j = y - 1; i < 8 && j > -1; j--, i++) {
                if (b[i][j] == 'O') {
                    isOpp = true;
                }
                else if (b[i][j] == 'I' && isOpp) {
                    for(int k = i , t = j ; k > x && t < y ; k-- , t++) {
                        b[k][t] = 'I';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
            for (int i = x - 1 , j = y + 1; i > -1 && j < 8; j++, i--) {
                if (b[i][j] == 'O') {
                    isOpp = true;
                }
                else if (b[i][j] == 'I' && isOpp) {
                    for(int k = i , t = j ; k < x && t > y ; k++ , t--) {
                        b[k][t] = 'I';
                    }
                    rev = true;
                    break;
                }
                else {
                    isOpp = false;
                    break;
                }
            }
        }
        return rev;
    }
    void completeBoard(String str) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(OthelloActivity.this);
        final View customLayout = getLayoutInflater().inflate(R.layout.alert_dialog_layout, null);
        alertDialog.setView(customLayout);
        alertDialog.setTitle(str);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(OthelloActivity.this,"Ok",Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }
}
