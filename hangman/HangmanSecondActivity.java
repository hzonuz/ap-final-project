package com.example.apfinalproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class HangmanSecondActivity extends AppCompatActivity implements View.OnClickListener {
    static boolean firstRound = true;
    char guess;
    StringBuilder showInView = new StringBuilder("");
    TextView attemptView;
    TextView ans;
    Button button;
    int counter;
    Intent intent;
    static int p1Score = 0;
    static int p2Score = 0;
    String p1w = "P1 Win !";
    String p2w = "P2 Win !";
    String draw = "Draw !";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hangman_guess_word);
        attemptView = findViewById(R.id.remain_hangman);
        ans = findViewById(R.id.ans_hangman);
        button = findViewById(R.id.submit_word);
        counter = 0;
        for (int q = 0; q < HangmanFirstActivity.attempt * 2; q++) {
            if(q % 2 == 0)
                showInView.append("_");
            else
                showInView.append(" ");
        }
        attemptView.setText("Your Chances : " + Integer.toString(HangmanFirstActivity.attempt - counter));
        ans.setText(showInView);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        EditText editText = findViewById(R.id.guess_hangman);
        try {
            String str = HangmanFirstActivity.answer;
            if (counter < HangmanFirstActivity.attempt) {
                guess = editText.getText().charAt(0);
                if (str.indexOf(guess) == -1) {
                    counter++;
                } else {
                    for (int i = 0; i < str.length(); i++) {
                        if (str.charAt(i) == guess) {
                            showInView.setCharAt(i * 2,guess);
                        }
                    }
                }
                ans.setText(showInView);
                attemptView.setText("Your Chances : " + Integer.toString(HangmanFirstActivity.attempt - counter));
                if (showInView.indexOf("_") == -1) {
                    if (firstRound) {
                        Toast.makeText(this, p1w , Toast.LENGTH_SHORT);
                        firstRound = false;
                        p1Score++;
                        intent = new Intent(this, HangmanFirstActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(this, p2w , Toast.LENGTH_SHORT);
                        if(p1Score == 1) {
                            alertDialog(draw);
                        } else {
                            alertDialog(p2w);
                        }
                    }
                }
            }
            if (firstRound && counter == HangmanFirstActivity.attempt) {
                Toast.makeText(this, p2w , Toast.LENGTH_SHORT);
                firstRound = false;
                p2Score++;
                intent = new Intent(this, HangmanFirstActivity.class);
                startActivity(intent);
            } else if(!firstRound && HangmanFirstActivity.attempt == counter){
                Toast.makeText(this, p1w , Toast.LENGTH_SHORT);
                if(p2Score == 1) {
                    alertDialog(draw);
                } else {
                    alertDialog(p1w);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void alertDialog (String str) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(HangmanSecondActivity.this);
        final View customLayout = getLayoutInflater().inflate(R.layout.alert_dialog_layout, null);
        alertDialog.setView(customLayout);
        alertDialog.setTitle(str);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(HangmanSecondActivity.this,"Ok",Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }
}
