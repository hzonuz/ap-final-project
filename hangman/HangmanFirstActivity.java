package com.example.apfinalproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class HangmanFirstActivity extends AppCompatActivity {
    static String answer;
    static int attempt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hangman_word_input);
        final EditText editText = findViewById(R.id.first_word);
        Button button = findViewById(R.id.hangman_submit);
        TextView textView = findViewById(R.id.round_number);
        final Intent intent = new Intent(this,HangmanSecondActivity.class);
        if(HangmanSecondActivity.firstRound) {
            textView.setText("Round 1");
        } else {
            textView.setText("Round 2");
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer = editText.getText().toString();
                attempt = answer.length();
                startActivity(intent);
            }
        });
    }
}
