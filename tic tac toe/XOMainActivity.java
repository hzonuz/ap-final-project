package com.example.apfinalproject;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class XOMainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button[][] buttons = new Button[3][3];
    private int round;
    private boolean p1Turn = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xo_layout);
        round = 0;
        for(int i = 0 ; i < 3 ;i++) {
            for(int j = 0 ; j < 3 ; j++) {
                String bId = "xo_button_" + i + j;
                int res = getResources().getIdentifier(bId,"id",getPackageName());
                buttons[i][j] = findViewById(res);
                buttons[i][j].setOnClickListener(this);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(!((Button) v).getText().equals("")) {
            return;
        }
        if(p1Turn) {
            ((Button) v).setText("X");
        } else {
            ((Button) v).setText("O");
        }
        p1Turn = !p1Turn;
        round++;
        if(checkForWin()) {
            if(p1Turn) {
                String p2w = "P2 Wins !";
                Toast.makeText(this,p2w ,Toast.LENGTH_SHORT).show();
                completeBoard(p2w);
            }
            else {
                String p1w = "P1 Wins !";
                Toast.makeText(this,p1w ,Toast.LENGTH_SHORT).show();
                completeBoard(p1w);
            }
        }
        if(round == 9) {
            String draw = "Draw !";
            Toast.makeText(this,draw,Toast.LENGTH_SHORT);
            completeBoard(draw);
        }
    }

    boolean checkForWin() {
        String [][] str = new String[3][3];
        for(int i = 0 ; i < 3 ; i++) {
            for(int j = 0 ; j < 3 ; j++) {
                str[i][j] = buttons[i][j].getText().toString();
            }
        }
        for(int i = 0; i < 3; i++) {
            if(str[i][0].equals(str[i][1]) && str[i][0].equals(str[i][2])
            && !str[i][0].equals("") && !str[2][0].equals("-")) {
                return true;
            }
        }
        for(int i = 0; i < 3; i++) {
            if(str[0][i].equals(str[1][i]) && str[0][i].equals(str[2][i])
                    && !str[0][i].equals("") && !str[2][0].equals("-")) {
                return true;
            }
        }
        if (str[0][0].equals(str[1][1]) && str[0][0].equals(str[2][2])
                && !str[0][0].equals("") && !str[2][0].equals("-"))
            return true;
        if (str[2][0].equals(str[1][1]) && str[2][0].equals(str[0][2])
                && !str[2][0].equals("") && !str[2][0].equals("-"))
            return true;
        return false;
    }
    void completeBoard(String str) {
        for(int i = 0; i < 3 ;i++) {
            for(int j = 0 ; j < 3 ; j++) {
                buttons[i][j].setText("-");
            }
        }
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(XOMainActivity.this);
        final View customLayout = getLayoutInflater().inflate(R.layout.alert_dialog_layout, null);
        alertDialog.setView(customLayout);
        alertDialog.setTitle(str);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(XOMainActivity.this,"Ok",Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }
}
