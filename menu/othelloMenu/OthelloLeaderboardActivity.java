package com.example.apfinalproject.othelloMenu;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.apfinalproject.LeaderboardAdapter;
import com.example.apfinalproject.MainActivity;
import com.example.apfinalproject.R;
import com.example.apfinalproject.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OthelloLeaderboardActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaderboard);
        Comparator<User> comparator = new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                if(o1.getScore() < o2.getScore()) {
                    return 1;
                } else if(o1.getScore() > o2.getScore()) {
                    return -1;
                }
                return 0;
            }
        };
        List<User> othelloLB = new ArrayList<>(MainActivity.friends);
        Collections.sort(othelloLB,comparator);
        RecyclerView recyclerView = findViewById(R.id.lb_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        OthelloLeaderboardAdapter ola = new OthelloLeaderboardAdapter(othelloLB);
        recyclerView.setAdapter(ola);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }
}
